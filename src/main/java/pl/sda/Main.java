package pl.sda;
import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        File file = new File("pan-tadeusz.txt");
        System.out.println(file.exists());
        System.out.println(file.getAbsolutePath());


        FileReader fileReader = null;
        try {

            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = null;

            // Map<SŁOWO, LICZBA_WYSTĄPIEŃ>
            Map<String, Integer> wordMap = new HashMap<String, Integer>();

            while((line = bufferedReader.readLine()) != null) {
                //System.out.println("Linijka tekstu to: " + line);

                line = line.replaceAll("[^a-zA-ZąĄęĘóÓśŚłŁżŻźŹćĆńŃ ]", "");
                line = line.toLowerCase();
                line = line.trim();
                String[] words = line.split(" ");
                //System.out.println(Arrays.toString(words));

                for (String word : words) {
                    //System.out.println(word);
                    if (!word.equals("")) {
                        if (wordMap.containsKey(word)) {
                            // zwiekszyc liczbe wystapien slowa o 1
                            Integer count = wordMap.get(word);
                            wordMap.put(word, count + 1);
                        } else {
                            // to nowe slowo, dodaj je do mapy
                            wordMap.put(word, 1);
                        }
                    }
                }
            }
            System.out.println(wordMap);
            List<Map.Entry<String, Integer>> entriesList =
                    new ArrayList<>(wordMap.entrySet());
            entriesList.sort(new Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> entry1, Map.Entry<String, Integer> entry2) {
                    String word1 = entry1.getKey();
                    String word2 = entry2.getKey();

                    Integer value1 = entry1.getValue();
                    Integer value2 = entry2.getValue();

                    return value2-value1;
                }
            });
            //System.out.println(entriesList);

            for (int i = 0; i < 5; i++) {
                System.out.println("Najpopularniejsze slowo numer " + (i+1) + " to " + entriesList.get(i));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}