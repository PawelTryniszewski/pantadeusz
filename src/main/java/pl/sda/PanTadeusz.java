package pl.sda;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


public class PanTadeusz {


    public static void main(String[] args) {
        File odczytPliku = new File("pan-tadeusz.txt");
        Map<String, Integer> mapa = new HashMap<>();
        List<String> lista = new ArrayList<>();
        String tmp = null;
        int licznikRymow = 0;
        String[] literkiTmp = new String[3];
        int licznikPojedynczych = 0;
        String pierwszyKlucz = "";
        String[] slowaTmp = new String[6];

        try {
            Scanner scanner = new Scanner((odczytPliku));

            while (scanner.hasNextLine()) {
                String[] slowa;
                String[] literki;
                String tekst = scanner.nextLine();
                tekst = tekst.toLowerCase();

                tekst = tekst.trim();
                tekst = tekst.replace(",", "");
                tekst = tekst.replace(".", "");
                tekst = tekst.replace(";", "");
                tekst = tekst.replace(":", "");
                tekst = tekst.replace("«", "");
                tekst = tekst.replace("»", "");
                tekst = tekst.replace("!", "");
                tekst = tekst.replace("?", "");
                tekst = tekst.replace("*", "");
                tekst = tekst.replace("—", "");
                tekst = tekst.replace("-", "");
                tekst = tekst.replace("(", "");
                tekst = tekst.replace(")", "");
                tekst = tekst.replace("…", "");
                tekst = tekst.trim();
                slowa = tekst.split(" ");
                literki = tekst.split("");
                if (literki.length > 2) {
                    if (literki[literki.length - 1].equals(literkiTmp[literkiTmp.length - 1])) {
                        if (literki[literki.length - 2].equals(literkiTmp[literkiTmp.length - 2])) {
                            if (literki[literki.length - 3].equals(literkiTmp[literkiTmp.length - 3]))
                                licznikRymow++;
                            lista.add(slowa[slowa.length - 1]);
                            lista.add(slowaTmp[slowaTmp.length - 1]);
                        }
                    }

                }
                literkiTmp = literki;
                slowaTmp = slowa;

                for (int i = 0; i < slowa.length; i++) {
                    if (!slowa[i].equals("")) {
                        int iloscPowy = 0;
                        for (String slowo : slowa) {
                            if (slowa[i].equals(slowo)) {
                                iloscPowy++;
                            }
                        }
                        if (mapa.isEmpty()) {
                            mapa.put(slowa[i], iloscPowy - 1);
                            pierwszyKlucz = slowa[i];
                        }
                        if (mapa.containsKey(slowa[i])) {
                            int temp = mapa.get(slowa[i]);
                            mapa.put(slowa[i], iloscPowy + temp);
                        } else {
                            mapa.put(slowa[i], iloscPowy);

                        }
                    }

                }
            }
            for (int i : mapa.values()) {
                if (i == 1) {
                    licznikPojedynczych++;
                }
            }
            System.out.println("Słowa pojedyncze " + licznikPojedynczych);
            System.out.println("Ilosć rymów " + licznikRymow);
            int licznikPowtRymów = 0;
            for (int i = 0; i < lista.size() - 2; i++) {
                for (int j = i + 2; j < lista.size() - 2; j++) {
                    if (lista.get(i).equals(lista.get(j))) {
                        if (lista.get(i + 1).equals(lista.get(j + 1))) {
                            licznikPowtRymów++;
                        }
                    }
                    if (lista.get(i).equals(lista.get(j + 1))) {
                        if (lista.get(i + 1).equals(lista.get(j))) {
                            licznikPowtRymów++;
                        }
                    }
                }

            }

            System.out.println("Ilość powtarzających się rymów " + licznikPowtRymów);
            Map<String, Integer> sorted = sortByValue(mapa);
            System.out.println("Najczęstsze "+sorted);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> mapa) {
        List<Map.Entry<K, V>> list = new LinkedList<>(mapa.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                return (e2.getValue()).compareTo(e1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (int i = 0; i < 5; i++) {

            result.put(list.get(i).getKey(),list.get(i).getValue());
        }



        return result;
    }


}
